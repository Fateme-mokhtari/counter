import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {


    render() {
        if (this.props.counters.length == 0) return <p>there is no counter</p>
        return (
            <React.Fragment>
                <button className="btn btn-primary btn-sm m-2" onClick={this.props.onReset}>Reset</button>
                {this.props.counters.map(counter =>
                    <Counter
                        key={counter.id}
                        counter={counter}
                        value={counter.value}
                        onReset={this.props.onReset}
                        onIncrement={this.props.onIncrement}
                        onDecrement={this.props.onDecrement}
                        onDelete={this.props.onDelete}

                    />)}
            </React.Fragment>

        );
    }
}

export default Counters;
