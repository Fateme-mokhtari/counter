import React, { Component } from 'react';

const Navbar = ({ countersTotal }) => {
    return (
        <nav className="navbar navbar-light bg-light">
            <span className="navbar-brand mb-0 h1">{countersTotal}</span>
        </nav>
    );
}

export default Navbar;