import React, { Component } from 'react';

class Counter extends Component {
    formatClass() {
        let classes = "badge  pr-2 pl-2 mr-3 text-center badge-";
        classes += this.props.value === 0 ? "warning" : "info";
        return classes
    }

    render() {

        return (
            <div>
                <span className={this.formatClass()}>{this.props.value}</span>
                <button className="btn btn-primary" onClick={() => this.props.onIncrement(this.props.counter)}>+</button>
                <button className="btn btn-secondary m-2"
                    onClick={() => this.props.onDecrement(this.props.counter)}
                    disabled={this.props.value === 0 ? "disabled" : ""}
                >-</button>
                <button className="btn btn-danger m-2"
                    onClick={() => this.props.onDelete(this.props.counter)} >X</button>
            </div>

        )
    }
}

export default Counter;