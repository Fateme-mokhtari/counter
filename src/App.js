import React, { Component } from 'react';
import Counters from './component/counters';
import Navbar from './component/navbar';
import './App.css';




class App extends Component {
  state = {
    counters: [
      { id: 1, value: 1 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]

  }

  handleIncrement = counter => {
    const counters = [...this.state.counters];
    let index = counters.indexOf(counter);


    counters[index].value++;
    this.setState({ counters });
    console.log({ ...counter })
  }

  handleDecrement = counter => {
    const counters = [...this.state.counters];
    let index = counters.indexOf(counter);


    counters[index].value--;
    this.setState({ counters });
    console.log({ ...counter })
  }
  handleDelet = counter => {
    let counters = [...this.state.counters];
    counters = counters.filter(c => c.id !== counter.id)
    this.setState({ counters })
  }
  handleCount() {

    let counters = [...this.state.counters];
    var total = null;
    for (let i = 0; i < counters.length; i++) {
      total += counters[i].value

    }
    return total
  }
  handleReset = () => {
    let counters = [...this.state.counters];
    counters.map(c => c.value = 0);
    this.setState({ counters })
  }
  render() {
    return (
      <React.Fragment>
        <Navbar countersTotal={this.handleCount()} />

        <div className="container mt-3">

          <Counters
            counters={this.state.counters}
            value={this.state.counters.value}
            onIncrement={this.handleIncrement}
            onDecrement={this.handleDecrement}
            onDelete={this.handleDelet}
            onReset={this.handleReset}
          />

        </div>
      </React.Fragment >
    );
  }

}

export default App;
